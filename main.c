#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <systemd/sd-bus.h>

int main(int argc, char *argv[]) {
	sd_bus_error error = SD_BUS_ERROR_NULL;
	sd_bus_message *m = NULL;
	sd_bus *bus = NULL;
	const char *path, *addr, *profile;
	int r;

	if (argc < 3) {
		fprintf(stderr, "usage: %s <bdaddr> <pbap|map|opp>\n", argv[0]);
		exit (1);
	}

	addr = argv [1];
	profile = argv [2];

	/* Connect to the system bus */
	r = sd_bus_open_user(&bus);
	if (r < 0) {
		fprintf(stderr, "Failed to connect to system bus: %s\n", strerror(-r));
		goto finish;
	}

	/* Issue the method call and store the respons message in m */
	r = sd_bus_call_method(bus,
						   "org.bluez.obex",         /* service to contact */
						   "/org/bluez/obex",        /* object path */
						   "org.bluez.obex.Client1", /* interface name */
						   "CreateSession",          /* method name */
						   &error,                   /* object to return error in */
						   &m,                       /* return message on success */
						   "sa{sv}",                 /* input signature */
						   addr,                     /* first argument */
						   1,                        /* dict size */
						   "Target",                 /* dict entry name */
						   "s", profile);            /* dict entry type and value */
	if (r < 0) {
		fprintf(stderr, "Failed to issue method call: %s\n", error.message);
		goto finish;
	}

	/* Parse the response message */
	r = sd_bus_message_read(m, "o", &path);
	if (r < 0) {
		fprintf(stderr, "Failed to parse response message: %s\n", strerror(-r));
		goto finish;
	}

	printf("%s\n", path);
	fflush(stdout);

	while (fgetc(stdin)) {
		usleep (1000);
	}

finish:
	sd_bus_error_free(&error);
	sd_bus_message_unref(m);
	sd_bus_unref(bus);

	return r < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
}
